/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polimorfismo_hija;

/**
 *
 * @author Benjamin
 */
public class GatoSiames extends Gato {
    
    private String tipoG;
    
    public GatoSiames(String tipoG,String Color, String tipoAnimal, String Nombre){
        super(Color,Nombre,tipoAnimal);
        this.tipoG=tipoG;
        
    }
    public String gettipoG(){
        return tipoG;
    }
    
     public String Mostrardatos(){
        return "Animal \nNombre: "+Nombre+"\ntipo de animal: "+tipoAnimal+"\nEl color es: "+Color+"\nTipo de gato: "+tipoG;
     }
    
}
