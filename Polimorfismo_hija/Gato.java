/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polimorfismo_hija;

/**
 *
 * @author Benjamin
 */
public class Gato extends animales{
    
    public String Color;
    
    public Gato(String Color,String tipoAnimal, String Nombre){
        super(tipoAnimal,Nombre);
        this.Color=Color;
        
    }
    
    public String getColor(){
        return Color;
    }
    
    public String Mostrardatos(){
        return "Animal \nNombre: "+Nombre+"\ntipo de animal: "+tipoAnimal+"\nEl color es: "+Color;
    }
}
