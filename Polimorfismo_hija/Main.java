/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polimorfismo_hija;

/**
 *
 * @author Benjamin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        animales tiposdeanimales[]=new animales[3];
        tiposdeanimales[0]= new animales("Domesticos","Gatos");
        tiposdeanimales[1]= new Gato("Negro","Domesticos","Gato");
        tiposdeanimales[2]= new GatoSiames("GatoSiames","Blanco","Domesticos","Gato");
        
        for(animales animal:tiposdeanimales){
            System.out.println(animal.Mostrardatos());
            System.out.println("");
        }
  
    }
    
}
