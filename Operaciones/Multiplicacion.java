/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

/**
 *
 * @author user
 */
public class Multiplicacion {
    private double multiplicando, multiplicador;
    public Multiplicacion(double multiplicacion1, double multiplicacion2){
        multiplicando=multiplicacion1;
        multiplicador=multiplicacion2;
        
    }
    public double operacion(){
        return multiplicando*multiplicador;
    }
    public double imprimir(){
        return operacion();
    }
}
